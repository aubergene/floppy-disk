/**
 * Core "driver" for the FAT12 file system.
 *
 * @author Alok Menghrajani
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

function Fat12FS(bytes) {
  /**
   * Takes the bytes as a string and
   * sets up all the data structures.
   *
   * Then calls the render function.
   */
  this.init = function(bytes) {
    this.bytes = [];
    var len = bytes.length;
    for (var i=0; i<len; i++) {
      this.bytes.push(bytes.charCodeAt(i) & 0xff);
    }
    this.data = new DataNode(0, len);
    this.map();
    this.render();
  }

  this.map = function() {
    // Map the MBR
    this.mbr = new MasterBootRecord();
    this.mbr.map(this);

    // Map the bootsector
    this.bootsector = new BootSector();
    this.bootsector.map(this);

    // Read the FAT
    this.fat = new Fat();
    this.fat.map(this);

    // Read the root directory and files
    this.directory = new Directory();
    this.directory.mapRoot(this);
  }

  this.render = function() {
    // Render the canvas
    // this.renderDiskOutline(this.data.length);

    // this.mbr.render();
    this.renderCanvas(this.mbr.data, "yellow");

    // this.bootsector.render();
    this.renderCanvas(this.bootsector.data, "red");

    // this.fat.render();
    this.renderCanvas(this.fat.data, "blue");
    this.renderCanvas(this.fat.data2, "lightblue");


    this.directory.render();
    this.directory.renderCanvas();
  }

  /**
   * Draws the outline of the disk on the canvas
   */
  this.renderDiskOutline = function(bytes) {
    var canvas = $('#bytes')[0];
    var inner_width = 500;
    canvas.width = inner_width + 2;
    canvas.height = Math.ceil(bytes/inner_width)+2;
    this.canvasSize = inner_width;

    var ctx = canvas.getContext('2d');
    var h1 = bytes/this.canvasSize|0;
    var h2 = bytes%this.canvasSize;
    ctx.lineWidth = 1;
    ctx.scale(1, 1);
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(this.canvasSize, 0);
    ctx.lineTo(this.canvasSize, h1);
    ctx.lineTo(this.canvasSize-h2, h1);
    ctx.lineTo(this.canvasSize-h2, h1+1);
    ctx.lineTo(0, h1+1);
    ctx.lineTo(0, 0);
    ctx.stroke();
  }

  this.renderCanvas3 = function(data, style) {
    var canvas = $('#bytes')[0];
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = style;

    // console.log(data.start, data.length, offsetToPosition(data.start))

    for (var i=data.start; i<data.start + data.length; i++) {
      var x = (i%this.canvasSize) + 1;
      var y = (i/this.canvasSize|0) + 1;
      ctx.fillRect(x, y, 1, 1);
    }

    // this.renderCanvas2(data, style)
  }

  var tracks = 90
  var sectors = 18
  // var sectors = 8
  var bytesPerSector = 512
  var bytesPerTrack = bytesPerSector * sectors

  function offsetToPosition(offset) {
    return {
      track: Math.floor(offset / bytesPerTrack),
      sector: Math.floor((offset % bytesPerTrack) / bytesPerSector),
      offset: ((offset % bytesPerTrack) % bytesPerSector)
    }
  }

  var frameCounter = 0

  this.renderCanvas = function(data, style) {
    var disk = document.getElementById('disk');
    var x = disk.width / 2, y = disk.height / 2;

    var ctx = disk.getContext('2d');

    function arc(startAngle, endAngle, innerRadius, outerRadius) {
      ctx.beginPath();
      ctx.arc(x, y, outerRadius, startAngle, endAngle, false);
      ctx.arc(x, y, innerRadius, endAngle, startAngle, true);
      ctx.closePath()
    }


    var τ = Math.PI * 2

    var byteAngle = τ * (1 / bytesPerTrack)
    // var finishAngle = τ * ((data.start + data.length) / bytesPerTrack)

    // console.log(data.start, data.length, offsetToPosition(data.start))

    var diskOuterRadius = Math.min(disk.width, disk.height) * .5 * .9
    var diskInnerRadius = diskOuterRadius * .3
    var trackRadius = (diskOuterRadius - diskInnerRadius) / tracks // distance from center
    var trackWidth = trackRadius * 0.8 // actual width of track arc


    // console.log(startAngle, track)

    // ctx.fillStyle = "rgba(0,0,0,0.2)";

    ctx.globalAlpha = 0.8


    setTimeout(function() {
      // ctx.fillStyle = "white";
      ctx.fillStyle = style;
      // ctx.strokeStyle = style;


      for (var i=data.start; i<data.start + data.length; i++) {

        var pos = offsetToPosition(i)
        var startAngle = τ * (pos.sector / sectors) + pos.offset * byteAngle
        var track = pos.track

        track = diskInnerRadius + track * trackRadius

    //   console.log(data.start)
    //   // renderByte(12, 10, i, data[i], "blah")
    //   // var x = (i%this.canvasSize) + 1;
    //   // var y = (i/this.canvasSize|0) + 1;
    //   // ctx.fillRect(x, y, 1, 1);

    //   // ctx.fillStyle = content.charCodeAt(bit) === 48 ? '#000' : '#fff'

    //   // console.log(startAngle)

        arc(startAngle, startAngle + byteAngle, track, track + trackWidth)
        // startAngle += byteAngle
        // finishAngle += byteAngle

        ctx.fill()
        // ctx.stroke();
      }
    }, frameCounter * 1)
    frameCounter++
  }




  /**
   * Given a cluster, uses the FAT to build a list of
   * DataNodes.
   */
  this.clusterToData = function(cluster) {
    var r = [];
    do {
      var offset =
        this.directory.root.clusters[0].start +
        this.directory.root.clusters[0].length +
        (cluster - 2) * 512;
      r.push(this.data.slice(offset, 512));
      cluster = this.fat.getNextCluster(cluster);
    } while (cluster != null);

    return r;
  }

  this.init(bytes);
}

