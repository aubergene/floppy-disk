'use strict';

var	tau = Math.PI * 2;

var floppy = document.getElementById('floppy'),
	ctx = floppy.getContext('2d'),
	size = Math.min(floppy.offsetWidth, floppy.offsetHeight),
	padding = 5,
	radius = (size / 2) - padding;

floppy.width = floppy.height = size;

var r0 = 65.0; // outer radius of disk
var r1 = 60.0; // outer writable radius of disk
var r2 = 30.0; // inner writable radius of disk
var r3 = 15.5; // inner radius of disk

r3 = (r3 / r0) * radius;
r2 = (r2 / r0) * radius;
r1 = (r1 / r0) * radius;
r0 = radius;

console.log(radius, r0, r1, r2, r3);

var sectors = 8,
	tracks = 80,
	bytesPerSector = 512,

	diskSize = sectors * tracks * bytesPerSector,

	x = 0, y = 0,

	sectorAngle = tau / sectors,
	byteAngle = sectorAngle / bytesPerSector,
	trackRadius = (r1 - r2) / tracks,
	byteAngle = sectorAngle / bytesPerSector,
	bitsPerByte = 8,
	bitAngle = byteAngle / bitsPerByte;

console.log('tracks', tracks, 'sectors', sectors, 'bytesPerSector', bytesPerSector, ' sectorAngle', sectorAngle, 'byteAngle', byteAngle);

ctx.translate(r0 + padding, r0 + padding);
ctx.rotate(-Math.PI * .5);

ctx.fillStyle = '#fff';

// renderByte(80, 0, 0, '10101010');

// function arc(startAngle, endAngle, innerRadius, outerRadius) {
// 	ctx.moveTo(x, y);
// 	ctx.beginPath();
// 	ctx.arc(x, y, outerRadius, startAngle, endAngle, false);
// 	ctx.closePath();
// 	ctx.stroke();
// }

function clearCanvas() {
	ctx.clearRect(-size / 2, -size / 2, size, size);

	ctx.save();
	ctx.fillStyle = 'rgba(0,0,0,0.8)';
	// ctx.fillStyle = '#000';

	ctx.beginPath();
	ctx.arc(0, 0, r3, 0, tau, true);
	ctx.moveTo(r0, 0);
	ctx.arc(0, 0, r0, 0, tau, false);
	ctx.fill();
	ctx.closePath();

	// ctx.strokeStyle = '#555';
	// ctx.beginPath();
	// ctx.arc(0, 0, r2, 0, tau, true);
	// ctx.moveTo(r1, 0);
	// ctx.arc(0, 0, r1, 0, tau, false);
	// ctx.stroke();
	// ctx.closePath();

	ctx.restore();
}

var file, buffer, uint8View;

var save = document.querySelector('#save')

function handleFiles(files) {
	var fileInput = document.querySelector('#file');
	var files = fileInput.files;

	// FIXME make mulitple files work properly
	for (var i = 0; i < files.length; i++) {
		file = files[i];

		if (file.size > diskSize) {
			console.log('File too large', file);
			return;
		}

		var reader = new FileReader();

		console.log(file);
		reader.readAsArrayBuffer(file);

		reader.onload = function() {
			console.log(reader);
			buffer = reader.result;
			uint8View = new Uint8Array(buffer);
			clearCanvas();
			writeBuffer();
			save.href = floppy.toDataURL()
			save.style.display = 'inline'
		};
	}
}

document.querySelector('#file').onchange = handleFiles;

function writeBuffer() {
	var ln = uint8View.length,
		track = 0,
		sector = 0,
		offset = 0,
		i = 0,
		str;


	console.time("writeBuffer")
	while (i < ln) {
		str = ('0000000' + uint8View[i].toString(2)).slice(-8);

		renderByte(track, sector, offset, str);

		offset++;

		if (offset == bytesPerSector) {
			offset = 0;
			sector++;
		}

		if (sector == sectors) {
			sector = 0;
			track++;
		}

		i++;
	}
	console.timeEnd("writeBuffer")
}


var inner, outer, startAngle, finishAngle, sectorByte;

function renderByte(track, sector, offset, content) {
	// console.log('renderByte', track, sector, offset, content, String.fromCharCode(cc));
	// console.log('renderByte', track, sector, offset, content);

	startAngle = sectorAngle * sector + (byteAngle * offset);
	finishAngle = startAngle + bitAngle;
	inner = r2 + (track * trackRadius);
	outer = inner + trackRadius * 0.8;

	for (var bit = 0; bit < bitsPerByte; bit++) {
		// console.log(content.charCodeAt(bit))
		if (content.charCodeAt(bit) !== 49) {
			continue;
		}

		// arc(startAngle, finishAngle, inner, outer);

		ctx.moveTo(0, 0);
		ctx.beginPath();
		// ctx.arc(x, y, outerRadius, startAngle, endAngle, false);
		ctx.arc(0, 0, outer, startAngle, finishAngle, false);
		ctx.arc(0, 0, inner, finishAngle, startAngle, true);
		ctx.closePath();
		ctx.fill();

		// console.log(inner, outer, startAngle, finishAngle);

		// ctx.fill();
		// ctx.stroke();

		startAngle += bitAngle,
		finishAngle += bitAngle;
	}
}

clearCanvas();
