Data Gotham 2013
================

(Hold up disk)
So, hands up if you used to use one of these?

(play on number of hands raised)

[When I was about 13 years old | Some time in about 1993], at school, I was given my first floppy disk. Now I was a bit unlucky since I had been allocated a somewhat temporamental computer on which the regular three and a half inch disk drive didn't work well, apparently because the computer got "too cold" as it was near to the window. However the machine also had a five and a quarter inch drive and so that was the disk I was given.

So, my floppy disk had a maximum formatted capacity of three hundred and sixty kilobytes, which even in 1993 was not a huge amount of space
 even in 1993. So I remember that my disk became full much more quickly than my class mate's who had the luxury of a one point four megabytes of storage.

